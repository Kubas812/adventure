package cz.vse.frolik.logika;


import cz.vse.frolik.logika.Hra;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra
 *
 * @author  Jakub Frolík
 * @version 1.0 — 2020-05-28
 */
public class HraTest {
    private Hra hra1;
    
    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
        hra1 = new Hra();
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    /***************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí
     * a v jaké aktuální místnosti se hráč nachází.
     * Při dalším rozšiřování hry doporučujeme testovat i jaké věci nebo osoby
     * jsou v místnosti a jaké věci jsou v batohu hráče.
     * 
     */
    @Test
    public void testPrubehHry() {
        assertEquals("Central_Park", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("jdi Fifth_Avenue");
        assertEquals(false, hra1.konecHry());
        assertEquals("Fifth_Avenue", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("seber metrocard");
        assertEquals(false, hra1.konecHry());
        assertEquals("Fifth_Avenue", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("jdi Columbus_Circle");
        assertEquals(false, hra1.konecHry());
        assertEquals("Columbus_Circle", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("použij metrocard");
        assertEquals(false, hra1.konecHry());
        assertEquals("Columbus_Circle", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("jeď metro");
        assertEquals(false, hra1.konecHry());
        assertEquals("metro", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("jdi Times_Square");
        assertEquals(false, hra1.konecHry());
        assertEquals("Times_Square", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("jdi Broadway");
        assertEquals(false, hra1.konecHry());
        assertEquals("Broadway", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("jdi East_Village");
        assertEquals(false, hra1.konecHry());
        assertEquals("East_Village", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("jdi Downtown");
        assertEquals(false, hra1.konecHry());
        assertEquals("Downtown", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("jdi Brooklyn_Bridge");
        assertEquals(false, hra1.konecHry());
        assertEquals("Brooklyn_Bridge", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("seber vstupenka");
        assertEquals(false, hra1.konecHry());
        assertEquals("Brooklyn_Bridge", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("jdi Downtown");
        assertEquals(false, hra1.konecHry());
        assertEquals("Downtown", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("jdi Wall_Street");
        assertEquals(false, hra1.konecHry());
        assertEquals("Wall_Street", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("jdi Battery_Park");
        assertEquals(false, hra1.konecHry());
        assertEquals("Battery_Park", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("jeď loď");
        assertEquals(false, hra1.konecHry());
        assertEquals("loď", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("jdi Liberty_Island");
        assertEquals(false, hra1.konecHry());
        assertEquals("Liberty_Island", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("použij vstupenka");
        assertEquals(false, hra1.konecHry());
        assertEquals("Liberty_Island", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("vylez Socha_Svobody");
        assertEquals(true, hra1.konecHry());
        assertEquals("Socha_Svobody", hra1.getHerniPlan().getAktualniProstor().getNazev());
        
        hra1.zpracujPrikaz("konec");
        assertEquals(true, hra1.konecHry());
    }
}
