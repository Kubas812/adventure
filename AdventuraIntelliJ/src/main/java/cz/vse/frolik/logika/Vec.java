package cz.vse.frolik.logika;

/**
 * Instance třídy {@code Vec} představují přenositelnost a název věci.
 * Metody dále porovnávají dvě věci a převádí jejich název na číslo.
 *
 * @author  Jakub Frolík
 * @version 1.0 — 2020-05-28
 */
public class Vec
{
    private String nazev;
    private boolean prenositelna;
    private boolean jePouzita = false;
    private boolean lzePouzit = false;
    
    /**
     * Metoda udává název věci a její přenositelnost.
     */
    public Vec(String nazev, boolean prenositelna){
        this.nazev = nazev;
        this.prenositelna = prenositelna;
    }
    
    /**
     * Metoda vrací název věci.
     * @return název věci
     */
    public String getNazev(){
        return nazev;
    }

    /**
     * Metoda vrací informaci o tom, zda je věc přenositelná.
     * @return true, pokud přenositelná, jinak false
     */
    public boolean jePrenositelna(){
        return prenositelna;
    }
    
    /**
     * Metoda porovnává dvě věci, jestli jsou stejné.
     * @param o object, který se má porovnávat s aktuálním
     * @return true, pokud jsou stejné, jinak false
     */
    @Override
    public boolean equals(Object o){
        if(this == o){
            return true;
        }   
    
        if(!(o instanceof Vec)){
            return false;
        }
    
        Vec druhy = (Vec) o;
        return (java.util.Objects.equals(this.nazev, druhy.getNazev()));
    }
    
    /**
     * Metoda přepisuje název na číslo a následně porovnává.
     * @return výsledek čísla
     */
    @Override
    public int hashCode(){
        int vysledek = 3;
        int hashNazvu = java.util.Objects.hashCode(this.nazev);
        vysledek = (hashNazvu + vysledek) * 27;
        return vysledek;
    }
    
    /**
     * Metoda vrací, zda lze věc použít.
     * @return false, pokud není nastaveno u metody jinak
     */
    public boolean lzePouzit(){ return lzePouzit; }
    
    /**
     * Metoda nastavuje příkaz na to, aby šel předmět použít.
     */
    public void setLzePouzit(boolean lzePouzit){
        this.lzePouzit = lzePouzit;
    }
    
    /**
     * Metoda vrací, zda je věc použita.
     * @return false, pokud není nastaveno u metody jinak
     */
    public boolean jePouzita(){
       return jePouzita;
    }
    
    /**
     * Metoda nastavuje příkaz na to, aby byl předmět použit.
     */
    public void setJePouzita(boolean jePouzita){
        this.jePouzita = jePouzita;
    }
}
