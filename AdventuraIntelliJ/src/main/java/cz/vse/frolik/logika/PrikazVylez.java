package cz.vse.frolik.logika;
    
/**
 * Instance třídy {@code PrikazVylez} implemetuje pro hru příkaz vylez.
 *
 * @author  Jakub Frolík
 * @version 1.0 — 2020-05-28
 */
public class PrikazVylez implements IPrikaz
{
    private static final String NAZEV = "vylez";
    private HerniPlan plan;
         
    /**
     * Konstruktor třídy.
     * @param plan herní plán, ve kterém se dá vylézt na místa
     */
    public PrikazVylez(HerniPlan plan){
         this.plan = plan;
    }
    
    /**
     * Provádí příkaz "vylez". Zkouší vylézt na dané místo. Pokud se na dané místo vylézt nedá,
     * vypíše chybové hlášení.
     * @param parametry - jako  parametr obsahuje jméno místa, kam má hráč vylézt
     * @return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0){
            return "Kam mám vylézt? Musíš zadat jméno místa.";
        }
        String smer = parametry[0];
        Prostor sousedniProstor = plan.getAktualniProstor().vratSousedniProstor(smer);
        Batoh batoh = plan.getBatoh();
        
        if (sousedniProstor == null){
            return "Tam se odsud vylézt nedá.";
        }
        else{
            if (sousedniProstor.lzeVylezt()){        
                if (batoh.obsahujePouzite("vstupenka")){
                    plan.setAktualniProstor(sousedniProstor);
                    return sousedniProstor.dlouhyPopis();
                }
                else{
                    return "Tam se zatím vylézt nedá. Nejprve najdi a použij vstupenku.";
                }
            }
            else{
                return "Tam se vylézt nedá. Zkus použít jiný příkaz.";
            }
        }
    }
    
    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání).
     * @return název příkazu
     */
    @Override
    public String getNazev(){
        return NAZEV;
    }
}
