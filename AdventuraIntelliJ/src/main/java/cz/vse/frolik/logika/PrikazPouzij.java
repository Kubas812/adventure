package cz.vse.frolik.logika;

/**
 * Instance třídy {@code PrikazPouzij} implemetuje pro hru příkaz použij.
 *
 * @author  Jakub Frolík
 * @version 1.0 — 2020-05-28
 */
public class PrikazPouzij implements IPrikaz
{
    private static final String NAZEV = "použij";
    private HerniPlan plan;
    
    /**
     * Konstruktor třídy.
     * @param plan herní plán, ve kterém se dá použít předmět z batohu
     */
    public PrikazPouzij(HerniPlan plan){
        this.plan = plan;
    }

    /**
     * Provádí příkaz "použij". Zkouší použít věc, která je v batohu. Pokud věc není v batohu,
     * vypíše chybové hlášení. 
     * @param parametry - jako  parametr obsahuje jméno věci, kterou má použít
     * @return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry){
        String vyhodnoceni = "";
        if (parametry.length == 0){
            vyhodnoceni = "Co mám použít? Musíš zadat jméno věci.";
        }
        else{
            String nazevVeci = parametry[0];
            Batoh batoh = plan.getBatoh();
        
            if (batoh.obsahujeVec(nazevVeci)){
                Vec vec = batoh.vratHodnotu(nazevVeci);
                if (vec.lzePouzit()){
                    if (batoh.obsahujeVec("metrocard")){
                        vec.setJePouzita(true);
                        boolean pouzitaVec = plan.getBatoh().vlozDoPouzitych(vec);
                        if (pouzitaVec){
                            vyhodnoceni = "Použil jsi metrocard. Nyní můžeš jezdit dopravními prostředky.";
                        }
                        batoh.vyndejZBatohu(nazevVeci);
                    }
                    
                    if (batoh.obsahujeVec("vstupenka")){
                        vec.setJePouzita(true);
                        boolean pouzitaVec = plan.getBatoh().vlozDoPouzitych(vec);
                        if (pouzitaVec){
                            vyhodnoceni = "Použil jsi vstupenku. Nyní můžeš na Sochu Svobody.";
                        }
                        batoh.vyndejZBatohu(nazevVeci);
                    }
                }
                else{
                    vyhodnoceni = "Tato věc nelze použít.";
                }
                
            }
            else{
                vyhodnoceni = "Tato věc se nenachází v batohu.";
            }
        }
        return vyhodnoceni;
    }
    
    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání) .
     * @return název příkazu
     */
    @Override
    public String getNazev(){
        return NAZEV;
    }
}
