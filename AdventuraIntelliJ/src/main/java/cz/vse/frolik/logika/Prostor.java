package cz.vse.frolik.logika;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import java.util.*;

/**
 * Trida Prostor - popisuje jednotlivé prostory (místnosti) hry
 *
 * Tato třída je součástí jednoduché textové hry.
 *
 * "Prostor" reprezentuje jedno místo (místnost, prostor, ..) ve scénáři hry.
 * Prostor může mít sousední prostory připojené přes východy. Pro každý východ
 * si prostor ukládá odkaz na sousedící prostor.
 *
 * @author  Jakub Frolík
 * @version 1.0 — 2020-05-28
 */
public class Prostor {
    private String nazev;
    private String popis;
    private Set<Prostor> vychody;
    private Set<Vec> obsahMistnosti;
    private boolean lzeVylezt = false;  //přidává příkazy k místu
    private boolean lzeJit = true;  //přidává příkazy k místu
    private boolean lzeJet = false;  //přidává příkazy k místu
    public String description;


    /**
     * Vytvoření prostoru se zadaným popisem, např. "kuchyň", "hala", "trávník
     * před domem"
     *
     * @param nazev nazev prostoru, jednoznačný identifikátor, jedno slovo nebo
     * víceslovný název bez mezer.
     * @param popis Popis prostoru.
     */
    public Prostor(String nazev, String popis) {
        this.nazev = nazev;
        this.popis = popis;
        vychody = new HashSet<>();
        obsahMistnosti = new HashSet<>();
    }

    /**
     * Definuje východ z prostoru (sousední/vedlejsi prostor). Vzhledem k tomu,
     * že je použit Set pro uložení východů, může být sousední prostor uveden
     * pouze jednou (tj. nelze mít dvoje dveře do stejné sousední místnosti).
     * Druhé zadání stejného prostoru tiše přepíše předchozí zadání (neobjeví se
     * žádné chybové hlášení). Lze zadat též cestu ze do sebe sama.
     *
     * @param vedlejsi prostor, který sousedi s aktualnim prostorem.
     *
     */
    public void setVychod(Prostor vedlejsi) {
        vychody.add(vedlejsi);
    }

    /**
     * Metoda equals pro porovnání dvou prostorů. Překrývá se metoda equals ze
     * třídy Object. Dva prostory jsou shodné, pokud mají stejný název. Tato
     * metoda je důležitá z hlediska správného fungování seznamu východů (Set).
     *
     * Bližší popis metody equals je u třídy Object.
     *
     * @param o object, který se má porovnávat s aktuálním
     * @return hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */
    @Override
    public boolean equals(Object o) {
        // porovnáváme zda se nejedná o dva odkazy na stejnou instanci
        if (this == o) {
            return true;
        }
        // porovnáváme jakého typu je parametr 
        if (!(o instanceof Prostor)) {
            return false;    // pokud parametr není typu Prostor, vrátíme false
        }
        // přetypujeme parametr na typ Prostor 
        Prostor druhy = (Prostor) o;

        //metoda equals třídy java.util.Objects porovná hodnoty obou názvů. 
        //Vrátí true pro stejné názvy a i v případě, že jsou oba názvy null,
        //jinak vrátí false.

        return (java.util.Objects.equals(this.nazev, druhy.nazev));
    }

    /**
     * metoda hashCode vraci ciselny identifikator instance, ktery se pouziva
     * pro optimalizaci ukladani v dynamickych datovych strukturach. Pri
     * prekryti metody equals je potreba prekryt i metodu hashCode. Podrobny
     * popis pravidel pro vytvareni metody hashCode je u metody hashCode ve
     * tride Object
     */
    @Override
    public int hashCode() {
        int vysledek = 3;
        int hashNazvu = java.util.Objects.hashCode(this.nazev);
        vysledek = 27 * vysledek + hashNazvu;
        return vysledek;
    }

    /**
     * Vrací název prostoru (byl zadán při vytváření prostoru jako parametr
     * konstruktoru)
     *
     * @return název prostoru
     */
    public String getNazev() {
        return nazev;
    }

    /**
     * Vrací "dlouhý" popis prostoru, který může vypadat následovně: Jsi v
     * mistnosti/prostoru vstupni hala budovy VSE na Jiznim meste. vychody:
     * chodba bufet ucebna
     *
     * @return Dlouhý popis prostoru
     */
    public String dlouhyPopis() {
        return "Nacházíš se na místě: " + popis + ".\n"
                + popisVychodu() + "\n"
                + "Věci: " + seznamVeci();
    }

    /**
     * Vrací textový řetězec, který popisuje sousední východy, například:
     * "vychody: Times Square ".
     *
     * @return Popis východů - názvů sousedních prostorů
     */
    private String popisVychodu() {
        String vracenyText = "Východy: ";
        for (Prostor sousedni : vychody) {
            vracenyText += " " + sousedni.getNazev();
        }
        return vracenyText;
    }

    /**
     * Metoda vrací, zda se věc nachází v místnosti.
     * @return true, pokud se věc nachází v místnosti, jinak false
     */
    public boolean obsahujeVec(String nazevVeci) {
        for (Vec neco : obsahMistnosti) {
            if(neco.getNazev().equals(nazevVeci)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Metoda vybere věc. Pokud je věc přenositelná,
     * odstraní vybranou věc z místnosti, jinak vrátí hodnotu null.
     * @return vybraná věc
     */
    public Vec vyberVec(String nazevVeci) {
        Vec vybranaVec = null;
        for (Vec neco : obsahMistnosti) {
            if(neco.getNazev().equals(nazevVeci)) {
                vybranaVec = neco;
            }
        }

        if (vybranaVec != null) {
            if(vybranaVec.jePrenositelna()) {
                obsahMistnosti.remove(vybranaVec);
            }
            else {
                vybranaVec = null;
            }
        }
        return vybranaVec;
    }

    /**
     * Metoda přidává věci do místnosti.
     */
    public void vlozVec(Vec neco){
        obsahMistnosti.add(neco);
    }

    /**
     * Metoda vrací seznam věcí.
     * @return seznam věcí
     */
    private String seznamVeci() {
        String seznam = "";
        for (Vec neco: obsahMistnosti) {
            seznam = seznam + neco.getNazev() + " ";
        }
        return seznam;
    }

    /**
     * Vrací prostor, který sousedí s aktuálním prostorem a jehož název je zadán
     * jako parametr. Pokud prostor s udaným jménem nesousedí s aktuálním
     * prostorem, vrací se hodnota null.
     *
     * @param nazevSouseda Jméno sousedního prostoru (východu)
     * @return Prostor, který se nachází za příslušným východem, nebo hodnota
     * null, pokud prostor zadaného jména není sousedem.
     */
    public Prostor vratSousedniProstor(String nazevSouseda) {
        List<Prostor>hledaneProstory =
            vychody.stream()
                   .filter(sousedni -> sousedni.getNazev().equals(nazevSouseda))
                   .collect(Collectors.toList());
        if(hledaneProstory.isEmpty()){
            return null;
        }
        else {
            return hledaneProstory.get(0);
        }
    }

    /**
     * Vrací kolekci obsahující prostory, se kterými tento prostor sousedí.
     * Takto získaný seznam sousedních prostor nelze upravovat (přidávat,
     * odebírat východy) protože z hlediska správného návrhu je to plně
     * záležitostí třídy Prostor.
     *
     * @return Nemodifikovatelná kolekce prostorů (východů), se kterými tento
     * prostor sousedí.
     */
    public Collection<Prostor> getVychody() {
        return Collections.unmodifiableCollection(vychody);
    }

    /**
     * Metoda vrací, zda lze na místo vylézt.
     * @return false, pokud není nastaveno u metody jinak
     */
    public boolean lzeVylezt(){
       return lzeVylezt;
    }

    /**
     * Metoda nastavuje příkaz na to, zda na toto místo půjde vylézt.
     */
    public void setZdeLzeVylezt(boolean lzeVylezt){
        this.lzeVylezt = lzeVylezt;
    }

    /**
     * Metoda vrací, zda lze na místo jít.
     * @return true, pokud není nastaveno u metody jinak
     */
    public boolean lzeJit(){
       return lzeJit;
    }

    /**
     * Metoda nastavuje příkaz na to, zda lze na místo jít.
     */
    public void setZdeLzeJit(boolean lzeJit){
        this.lzeJit = lzeJit;
    }

    /**
     * Metoda vrací, zda lze na místo jet.
     * @return false, pokud není nastaveno u metody jinak
     */
    public boolean lzeJet(){
       return lzeJet;
    }

    /**
     * Metoda nastavuje příkaz na to, zda lze na místo jet.
     */
    public void setZdeLzeJet(boolean lzeJet){
        this.lzeJet = lzeJet;
    }

    public Collection<Vec> getThings() {
        return Collections.unmodifiableCollection(obsahMistnosti);
    }

    public String description() {
        return popis;
    }

    public Prostor getExitArea(String areaName)
    {
        return vychody.stream()
                .filter(exit -> exit.getNazev().equals(areaName))
                .findAny().orElse(null);
    }

}
