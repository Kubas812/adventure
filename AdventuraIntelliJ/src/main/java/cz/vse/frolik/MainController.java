package cz.vse.frolik;

import cz.vse.frolik.logika.*;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

/**
 * Třída MainController popisuje jednotlivé metody, které se vyvolávají ve hře.
 */
public class MainController {

    public TextArea textOutput;
    public TextField textInput;
    private IHra hra;

    public Label locationName;
    public Label locationDescription;
    public ImageView background;

    public VBox exits;
    public VBox items;
    public VBox backpack;
    public VBox used;

    private Stage primaryStage;

    public void init(IHra hra, Stage primaryStage) {
        this.hra = hra;
        this.primaryStage = primaryStage;
        update();
    }

    /**
     * Metoda pro aktualizaci věcí ve hře.
     */
    private void update() {
        String location = getAktualniProstor().getNazev();
        locationName.setText(location);
        String description = getAktualniProstor().description();
        locationDescription.setText(description);

        InputStream stream = getClass().getClassLoader().getResourceAsStream(location + ".jpg");
        assert stream != null;
        Image img = new Image(stream);
        background.setImage(img);
        background.setFitHeight(800);
        background.setFitWidth(1200);


        updateExits();
        updateItems();
        updateBackpack();
        updateUsed();
    }


    /**
     * Metoda pro vrácení a zobrazení předmětů nacházejících se v dané lokaci.
     */
    private void updateItems() {
        Collection<Vec> itemList = getAktualniProstor().getThings();
        items.getChildren().clear();

        for (Vec item : itemList) {
            String itemName = item.getNazev();
            Label itemLabel = new Label(itemName);

            picture(itemName, itemLabel);

            if(item.jePrenositelna()) {
                itemLabel.setCursor(Cursor.HAND);
                itemLabel.getStyleClass().add("background");
                itemLabel.setOnMouseClicked(event -> executeCommand("seber "+itemName));
            } else {
                itemLabel.setTooltip(new Tooltip("Tuto věc nelze sebrat"));
            }

            items.getChildren().add(itemLabel);
        }
    }

    /**
     * Metoda pro zjednodušení opakujících se příkazů.
     */

    private void picture(String itemName, Label itemLabel) {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(itemName + ".jpg");
        assert stream != null;
        Image img = new Image(stream);
        ImageView imageView = new ImageView(img);
        imageView.setFitWidth(60);
        imageView.setFitHeight(40);
        itemLabel.setGraphic(imageView);
    }

    /**
     * Metoda pro vrácení a zobrazení předmětů přidaných do batohu.
     */
    private void updateBackpack() {
        Collection<Vec> backpackList = getSeznamVeci().getThingsInBackpack();
        backpack.getChildren().clear();

        for (Vec backpackThing : backpackList) {
            String backpackThingName = backpackThing.getNazev();
            Label backpackLabel = new Label(backpackThingName);
            backpackLabel.setCursor(Cursor.HAND);
            backpackLabel.setTooltip(new Tooltip("Kliknutím použiješ předmět"));
            backpackLabel.getStyleClass().add("background");

            picture(backpackThingName, backpackLabel);

            backpackLabel.setOnMouseClicked(event -> executeCommand("použij "+backpackThingName));

            backpack.getChildren().add(backpackLabel);
        }
    }

    /**
     * Metoda pro vrácení a zobrazení použitých předmětů.
     */
    private void updateUsed() {
        Collection<Vec> usedList = getSeznamVeci().getThingsUsed();
        used.getChildren().clear();

        for (Vec usedThing : usedList) {
            String usedThingName = usedThing.getNazev();
            Label usedLabel = new Label(usedThingName);
            usedLabel.setTooltip(new Tooltip("Předmět jsi už použil"));

            picture(usedThingName, usedLabel);


            used.getChildren().add(usedLabel);
        }
    }

    /**
     * Metoda pro vrácení a zobrazení východů.
     */
    private void updateExits() {
        Collection<Prostor> exitList = getAktualniProstor().getVychody();
        exits.getChildren().clear();

        for (Prostor prostor : exitList) {
            String exitName = prostor.getNazev();
            Label exitLabel = new Label(exitName);
            exitLabel.setCursor(Cursor.HAND);
            exitLabel.setTooltip(new Tooltip(prostor.description()));
            exitLabel.getStyleClass().add("background");

            picture(exitName, exitLabel);

            exitLabel.setOnMouseClicked(event -> {
                if (getAktualniProstor().getExitArea(exitName).lzeJit()) {
                    executeCommand("jdi " + exitName);}
                else if (getAktualniProstor().getExitArea(exitName).lzeJet()) {
                    executeCommand("jeď " + exitName);}
                else {
                    executeCommand("vylez " + exitName);
                }
            });

            exits.getChildren().add(exitLabel);
        }
    }

    /**
     * Metoda pro zjednodušení opakujících se příkazů.
     */
    private void executeCommand(String command) {
        String result = hra.zpracujPrikaz(command);
        textOutput.appendText(result + "\n\n");
        update();
    }

    /**
     * Metoda vrací aktuální prostor z herního plánu.
     * @return aktuální prostor
     */
    private Prostor getAktualniProstor() {
        return hra.getHerniPlan().getAktualniProstor();
    }

    /**
     * Metoda vrací seznam věcí v batohu.
     * @return seznam věcí
     */
    private Batoh getSeznamVeci() {
        return hra.getHerniPlan().getBatoh();
    }

    /**
     * Metoda pro zjednodušení opakujících se příkazů.
     */
    public void onInputKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            executeCommand(textInput.getText());
            textInput.setText("");
        }
    }

    /**
     * Metoda ukončí spuštěnou hru.
     */
    public void actionEndGame() {
        Platform.exit();
    }

    /**
     * Metoda vytvoří novou hru.
     */
    public void actionNewGame() {
        primaryStage.setTitle("Game");
        FXMLLoader loader = new FXMLLoader();
        InputStream stream = getClass().getClassLoader().getResourceAsStream("scene.fxml");
        Parent root = null;
        try {
            root = loader.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert root != null;
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setFullScreen(false);
        primaryStage.setHeight(800);
        primaryStage.setWidth(1200);
        MainController controller = loader.getController();
        IHra hra = new Hra();
        controller.init(hra, primaryStage);
        primaryStage.show();
    }
    /**
     * Metoda zobrazí uživatelskou příručku ke hře v novém okně.
     */
    public void gameInfo() {
        Stage secondStage = new Stage();
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.load( getClass().getResource("/tutorial.html").toString() );
        Scene scene = new Scene(webView,600,800);
        secondStage.setScene(scene);
        secondStage.setTitle("Tutorial");
        secondStage.show();
    }

    /**
     * Metoda zobrazí mapu hry v novém okně.
     */
    public void gameMap() {
        Stage secondStage = new Stage();
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.load( getClass().getResource("/Mapa.png").toString() );
        Scene scene = new Scene(webView,600,800);
        secondStage.setScene(scene);
        secondStage.setTitle("Mapa");
        secondStage.show();
    }
}