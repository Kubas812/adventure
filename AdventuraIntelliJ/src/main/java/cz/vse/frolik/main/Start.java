package cz.vse.frolik.main;

import cz.vse.frolik.logika.Hra;
import cz.vse.frolik.logika.IHra;
import cz.vse.frolik.MainController;
import cz.vse.frolik.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/*******************************************************************************
 * Třída  Start je hlavní třídou projektu,
 * který představuje jednoduchou textovou adventuru určenou k dalším úpravám a rozšiřování
 *
 * @author    Jarmila Pavlíčková
 * @version   ZS 2016/2017
 */
public class Start extends Application
{

    /***************************************************************************
     */
    public static void main(String[] args)
    {
        List<String> vstup = Arrays.asList(args);

        if(vstup.contains("text")) {
            IHra hra = new Hra();
            TextoveRozhrani ui = new TextoveRozhrani(hra);
            ui.hraj();
        } else {
            launch();
        }
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setFullScreen(false);
        primaryStage.setTitle("Hra");

        FXMLLoader loader = new FXMLLoader();
        InputStream stream = getClass().getClassLoader().getResourceAsStream("scene.fxml");
        Parent root = loader.load(stream);

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);

        MainController controller = loader.getController();
        IHra hra = new Hra();
        controller.init(hra, primaryStage);
        primaryStage.show();
    }

}
